from flask import Flask
from flask_restplus import Api
from flask_jwt import JWT

from security import authentication, identity
from Resource.reimbursement import Reimbursement
from Resource.check_reimburse import Check_reimburse
from Resource.reimburse_action import Action_reimburse
from Resource.filter_by_status import Status_filter
from db import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///reimbursement'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = "secretkey"

api = Api(app)
jwt = JWT(app,authentication,identity)

api.add_resource(Reimbursement,'/<int:id>/reimbursement')
api.add_resource(Check_reimburse,'/<int:id>/reimbursement/')
api.add_resource(Action_reimburse,'/<int:id>/reimbursement/action')
api.add_resource(Status_filter,'/<int:id>/reimbursement/action/')

@app.before_first_request
def create_table():
    db.create_all()

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=5002,debug=True)
