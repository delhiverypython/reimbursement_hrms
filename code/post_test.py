import urllib.request
import json      

data = {
	"super_id":30,
	"title":"vehicle",
	"reim_type":"travel",
	"date":"2019-04-06 00:00:00",
	"amount":200,
	"comment":"petrol bills"
    } 

myurl = "http://127.0.0.1:5000/2/reimbursement"
req = urllib.request.Request(myurl)
req.add_header('Content-Type', 'application/json; charset=utf-8')
jsondata = json.dumps(data)
jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
req.add_header('Content-Length', len(jsondataasbytes))
print(jsondataasbytes)
response = urllib.request.urlopen(req, jsondataasbytes)
print(response.read().decode('utf-8'))