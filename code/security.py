import json

import urllib.request

def authentication():
    pass

def identity(payload):
    user_id = payload['identity']
    url= f"http://localhost:5001/employees/{user_id}"
    resp = urllib.request.urlopen(url)
    resp = resp.read().decode('utf-8')
    resp = json.loads(resp)
    return resp