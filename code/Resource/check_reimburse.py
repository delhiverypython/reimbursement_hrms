from flask import request, jsonify
from flask_restplus import Resource
from flask_jwt import jwt_required, current_identity

from db import db
from Model.reimburse import Reimburse, ReimburseSchema

class Check_reimburse(Resource):


    @jwt_required()
    def get(self,id):
        title = request.args.get('title')
        reim_type = request.args.get('reim_type')
        if title and reim_type:
            rows = Reimburse.query.filter_by(title = title,id = id,reim_type = reim_type).all()
        elif title:
            rows = Reimburse.query.filter_by(title = title,id = id).all()
        elif reim_type:
            rows = Reimburse.query.filter_by(id = id,reim_type = reim_type).all()
        else:
            rows = Reimburse.query.filter_by(id = id).all()

        if rows:
            schema = ReimburseSchema(many = True,exclude=('id','super_id'))
            reimburse = schema.dump(rows)
            if reimburse[1]:
                return reimburse[1]
            return jsonify(reimburse.data)
        else:
            return {'message':'No entry found'}