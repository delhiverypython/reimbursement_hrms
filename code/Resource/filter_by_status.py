from flask import request, jsonify
from flask_restplus import Resource
from flask_jwt import jwt_required, current_identity

from db import db
from Model.reimburse import Reimburse, ReimburseSchema

class Status_filter(Resource):

    
    @jwt_required()
    def get(self,id):
        status = request.args.get('status')
        rows = Reimburse.query.filter_by(super_id = id,status = status).all()
        if rows:
            schema = ReimburseSchema(many = True,exclude=('id','super_id'))
            reimburse = schema.dump(rows)
            if reimburse[1]:
                return reimburse[1]
            return jsonify(reimburse.data)
        else:
            return {'message':'No entry found'}