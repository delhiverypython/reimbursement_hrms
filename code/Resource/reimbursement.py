from flask import request, jsonify
from flask_restplus import Resource
from flask_jwt import jwt_required, current_identity

from db import db
from Model.reimburse import Reimburse, ReimburseSchema

class Reimbursement(Resource):

    
    @jwt_required()
    def get(self, id):
        rows = Reimburse.query.filter_by(id = id).all()
        if rows:
            schema = ReimburseSchema(many = True,exclude=('id','super_id'))
            reimburse = schema.dump(rows)
            if reimburse[1]:
                return reimburse[1]
            return jsonify(reimburse.data)
        else:
            return {'message':'No entry found'}

    @jwt_required()
    def post(self,id):
        data = request.get_json()
        data['id'] = id
        schema = ReimburseSchema()
        reimburse = schema.load(data)
        try:
            db.session.add(reimburse.data)
            db.session.commit()
        except:
            return reimburse[1]
        
        return jsonify({'message':'Your response is added'})

    @jwt_required()
    def delete(self,id):
        data = request.get_json()
        r_id = data['r_id']
        row = Reimburse.query.filter_by(r_id = r_id).first() 
        try:
            db.session.delete(row)
            db.session.commit()
        except:
            return jsonify({'alert':'Some error occured or request is already deleted'})
        
        return jsonify({'message':'Your response is deleted'})