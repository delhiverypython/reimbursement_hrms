from flask import request, jsonify
from flask_restplus import Resource
from flask_jwt import jwt_required, current_identity

from Model.reimburse import Reimburse, ReimburseSchema
from db import db

class Action_reimburse(Resource):


    @jwt_required()
    def get(self, id):
        rows = Reimburse.query.filter_by(super_id = id,status = "pending").all()
        if rows:
            schema = ReimburseSchema(many = True,exclude=('id','super_id'))
            reimburse = schema.dump(rows)
            if reimburse[1]:
                return reimburse[1]
            return jsonify(reimburse.data)
        else:
            return {'message':'No pending request found'}

    @jwt_required()
    def put(self,id):
        data = request.get_json()
        r_id = data['r_id']
        row = Reimburse.query.filter_by(r_id = r_id).first() 
        try:
            row.status = data['status']
            db.session.commit()
            return jsonify({'message':'Your response is noted'})
        except:
            return jsonify({'alert':'Some error occured'})
        