import datetime

from marshmallow import Schema, fields, post_load, validates, ValidationError, validates_schema

from db import db

class Reimburse(db.Model):
    r_id = db.Column(db.Integer,primary_key = True,unique = True, nullable = True)
    id = db.Column(db.Integer, nullable = False)
    super_id = db.Column(db.Integer, nullable = False)
    title = db.Column(db.Text, nullable = False)
    reim_type = db.Column(db.Text, nullable = False)
    date = db.Column(db.DateTime, nullable = False)
    amount = db.Column(db.Float, nullable = False)
    comment = db.Column(db.Text, nullable = True)
    status = db.Column(db.Text, default='pending')

class ReimburseSchema(Schema):
    r_id = fields.Integer()
    id = fields.Integer()
    super_id = fields.Integer()
    title = fields.String()
    reim_type = fields.String()
    date = fields.Date()
    amount = fields.Float()
    comment = fields.String()
    status = fields.String()

    @post_load
    def create_reimburse(self, data):
        return Reimburse(**data)

    @validates('date')
    def check_date(self,value_date):
        dt = datetime.date.today()
        if value_date > dt:
            raise ValidationError('Entered date is not valid')

    @validates_schema
    def validate_amount(self, data, **kwargs):
        if data['amount'] <= 0:
            raise ValidationError('Amount should be positive')

        if data['reim_type'] == 'Fuel Reimbursement' and data['amount'] >1000:
            raise ValidationError('Amount should be less than Rs.1000')

        elif data['reim_type'] == 'Meal Expense' and data['amount'] >750:
            raise ValidationError('Amount should be less than Rs.750')
    
        elif data['reim_type'] == 'Mobile Reimbursement' and data['amount'] >250:
            raise ValidationError('Amount should be less than Rs.250')

        elif data['reim_type'] == 'Outstation Travel Expense' and data['amount'] >1500:
            raise ValidationError('Amount should be less than Rs.1500')
